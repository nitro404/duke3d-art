# Duke3D Art

[![NPM version][npm-version-image]][npm-url]
[![Build Status][build-status-image]][build-status-url]
[![Coverage Status][coverage-image]][coverage-url]
[![Known Vulnerabilities][vulnerabilities-image]][vulnerabilities-url]
[![Downloads][npm-downloads-image]][npm-url]

A module for reading Duke Nukem 3D Tiles.ART files.

## Usage

```javascript
var Art = require("duke3d-art");

TODO
```

## Installation

To install this module:
```bash
npm install duke3d-art
```

[npm-url]: https://www.npmjs.com/package/duke3d-art
[npm-version-image]: https://img.shields.io/npm/v/duke3d-art.svg
[npm-downloads-image]: http://img.shields.io/npm/dm/duke3d-art.svg

[build-status-url]: https://travis-ci.org/nitro404/duke3d-art
[build-status-image]: https://travis-ci.org/nitro404/duke3d-art.svg?branch=master

[coverage-url]: https://coveralls.io/github/nitro404/duke3d-art?branch=master
[coverage-image]: https://coveralls.io/repos/github/nitro404/duke3d-art/badge.svg?branch=master

[vulnerabilities-url]: https://snyk.io/test/github/nitro404/duke3d-art?targetFile=package.json
[vulnerabilities-image]: https://snyk.io/test/github/nitro404/duke3d-art/badge.svg?targetFile=package.json
